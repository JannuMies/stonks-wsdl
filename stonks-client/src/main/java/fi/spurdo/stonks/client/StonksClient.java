package fi.spurdo.stonks.client;


import fi.spurdo.stonks.soap.ws.Stonk;
import fi.spurdo.stonks.soap.ws.StonkService;
import fi.spurdo.stonks.soap.ws.StonkService_Service;

public class StonksClient  {

    private static StonkService service;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            service = new StonkService_Service().getStonkServiceSOAP();

            Stonk stonk = service.getStonk("Kange Sinep");
            System.out.println("Stonk name: " + stonk.getName());
            System.out.println("Stonk price: " + stonk.getPrice());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
