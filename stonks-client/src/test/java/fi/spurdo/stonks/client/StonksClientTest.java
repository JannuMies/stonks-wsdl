package fi.spurdo.stonks.client;

import static org.junit.jupiter.api.Assertions.assertThrows;

import com.sun.xml.ws.client.ClientTransportException;

import org.junit.jupiter.api.Test;

import fi.spurdo.stonks.soap.ws.StonkService;
import fi.spurdo.stonks.soap.ws.StonkService_Service;

public class StonksClientTest {

    private static StonkService service;

    @Test
    public void testClientFailsForWrongEndpoint() throws Exception {
        service = new StonkService_Service().getStonkServiceSOAP();
        assertThrows(ClientTransportException.class, () -> service.getStonk("foo"));
    }
}
