package fi.spurdo.stonks.server.health;

import jakarta.jws.WebMethod;
import jakarta.jws.WebService;

/**
 * A "Just write Java"-example of a trivial Web Service, without an
 * external definition file.
 */
@WebService(targetNamespace = "fi.spurdo.stonks.health")
public class HealthService {
    @WebMethod(operationName="Health")
        public String getHealth() {
            return "healthy";
        }
}

