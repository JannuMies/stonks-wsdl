package fi.spurdo.stonks.server;


import com.google.inject.Guice;
import com.google.inject.Injector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fi.spurdo.stonks.server.health.HealthService;
import fi.spurdo.stonks.server.ioc.ServerModule;
import fi.spurdo.stonks.soap.ws.StonkService;

import jakarta.xml.ws.Endpoint;

public class StonksMain {
    private static Logger LOGGER = LogManager.getLogger(StonksMain.class);

    public static void main(String[] args) {
        LOGGER.info("Starting STONKS-wsdl server");
        final Injector injector = Guice.createInjector(new ServerModule());

        Endpoint.publish("http://localhost:61234/health", injector.getInstance(HealthService.class));
        Endpoint.publish("http://localhost:61234/StonkService", injector.getInstance(StonkService.class));
    }
}
