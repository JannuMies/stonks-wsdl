package fi.spurdo.stonks.server;

import jakarta.jws.WebService;

import fi.spurdo.stonks.soap.ws.Stonk;
import fi.spurdo.stonks.soap.ws.StonkService;

@WebService(endpointInterface = "fi.spurdo.stonks.soap.ws.StonkService")
public class StonksServiceImpl implements StonkService {
	final static Stonk stonk = new Stonk();
	{
		stonk.setName("Kange Sinep");
		stonk.setPrice("420.69");
	}

	@Override
	public Stonk getStonk(String name) {
		return stonk;
	}
}

