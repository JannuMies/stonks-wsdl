package fi.spurdo.stonks.server.ioc;

import jakarta.inject.Singleton;

import com.google.inject.AbstractModule;

import fi.spurdo.stonks.server.StonksServiceImpl;
import fi.spurdo.stonks.soap.ws.StonkService;

public class ServerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(StonkService.class).to(StonksServiceImpl.class).in(Singleton.class);
    } 
}
